FROM alpine:3.12
LABEL description="Ansible on Alpine 3.12 and Python 3.7"

RUN apk -U add gcc make python3 python3-dev openssl-dev && \
    apk -U add py3-cffi py3-bcrypt py3-cryptography py3-pynacl py3-pip && \
    pip3 install --upgrade pip && \
    pip3 install ansible==2.9.14
    
CMD ["ansible", "--version"]